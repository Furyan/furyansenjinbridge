package FuryansEnjinBridge;

import java.io.File;
import java.util.UUID;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

import com.furyan.util.StartMetrics;

public class MyPlugin extends PluginBase
{
	private static final String MAJOR_VERSION = "0";
	private static final String MINOR_VERSION = "1";
	private static final String VERSION_TAG = "ALPHA";
	public static String PLUGIN_NAME = "FuryansEnjinBridge";
	public static String PLUGIN_TITLE = "Furyan's Enjin Bridge";
	public MC_Server Server = null;
	private MC_Player player;

	public PluginInfo getPluginInfo()
	{
		PluginInfo info = new PluginInfo();
		info.description = PLUGIN_TITLE + " v" + MAJOR_VERSION + "." + MINOR_VERSION + " " + VERSION_TAG + "";
		info.name = PLUGIN_NAME;
		info.version = MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG;

		return info;
	}

	public void onTick(int tickNumber)
	{
	}

	public void onStartup(MC_Server server)
	{
		Pl3xLibs.getScheduler().scheduleTask(PLUGIN_NAME, new StartMetrics(this.getPluginInfo()), 100);
		System.out.println("=-=-= " + PLUGIN_TITLE + " is starting up!");
		Server = server;

		System.out.println("=-=-= * Loading Configuration...");
		
		
	}

	public static File getPluginConfigDirectory()
	{
		File jar = new File(MyPlugin.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		File dir = new File(jar.getParent() + File.separator + PLUGIN_NAME + File.separator);
		if (!dir.exists())
		{
			dir.mkdirs();
		}

		return dir;
	}

	public void onShutdown()
	{
		System.out.println("=-=-= " + PLUGIN_NAME + " is shutting down!");

	}

	public void onPlayerJoin(MC_Player player)
	{
	}

	public void onPlayerRespawn(MC_Player player)
	{
	}

	public void onPlayerLogout(String playerName, UUID uuid)
	{
	}
}
